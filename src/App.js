import React, { Component } from 'react';
import './App.css';

class App extends Component {
  state = {
    todoList: [
      { done: false, label: 'Trabalho de API' },
      { done: false, label: 'Trabalho de HTML' },
      { done: true, label: 'Trabalho de JS' },
      { done: false, label: 'Desafio JS' },
      { done: true, label: 'Passar em OAC' },
    ],
  };

  toggleTodo = index => {
    const newList = this.state.todoList.map((el, idx) =>
      idx === index ? { ...el, done: !el.done } : el,
    );

    this.setState({ todoList: newList });
  };

  createTodo = event => {
    event.preventDefault();

    this.setState({
      todoList: [
        ...this.state.todoList,
        { done: false, label: event.target.label.value },
      ],
    });
  };

  render() {
    const { todoList } = this.state;

    return (
      <div className="app">
        <h1 className="app-title">Todo</h1>

        <ul className="app-list">
          {todoList.map((el, i) => (
            <li
              className={'app-item ' + (el.done ? 'app-item--done' : '')}
              key={el.label}
            >
              <span onClick={() => this.toggleTodo(i)}>{el.label}</span>
            </li>
          ))}
        </ul>

        <form className="app-form" onSubmit={this.createTodo}>
          <input
            className="app-input"
            name="label"
            type="text"
            placeholder="Description"
          />
          <input className="app-input" type="submit" value="Create New Todo" />
        </form>
      </div>
    );
  }
}

export default App;
